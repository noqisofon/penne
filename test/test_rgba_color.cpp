#include <gtest/gtest.h>

#include "penne/rgba_color.hxx"
#include "penne/rgba_color_ios.hxx"
#include "penne/rgba_color_utils.hxx"

class RGBAColorTest : public ::testing::Test {
 protected:
    RGBAColorTest();
    virtual ~RGBAColorTest();

    virtual void SetUp();

    virtual void TearDown();
    
};

RGBAColorTest::RGBAColorTest() {
}

RGBAColorTest::~RGBAColorTest() {
}

void RGBAColorTest::SetUp() {
}

void RGBAColorTest::TearDown() {
} 

TEST_F(RGBAColorTest, sanity_test) {
    penne::Color a_color( 0x00, 0x00, 0x00 );

    EXPECT_EQ( 0x00, a_color.r );
    EXPECT_EQ( 0x00, a_color.g );
    EXPECT_EQ( 0x00, a_color.b );
    EXPECT_EQ( 0xff, a_color.a );
}

TEST_F(RGBAColorTest, sanity_test2) {
    penne::rgba_color<std::uint8_t> a_color( 0x00, 0x00, 0x00 );

    EXPECT_EQ( 0x00, a_color.r );
    EXPECT_EQ( 0x00, a_color.g );
    EXPECT_EQ( 0x00, a_color.b );
    EXPECT_EQ( 0xff, a_color.a );
}

TEST_F(RGBAColorTest, from_rgba_color) {
    penne::Color a_color = penne::Color::fromRGBA( 0x00, 0x00, 0x00, 0xff );

    EXPECT_EQ( 0x00, a_color.r );
    EXPECT_EQ( 0x00, a_color.g );
    EXPECT_EQ( 0x00, a_color.b );
    EXPECT_EQ( 0xff, a_color.a );
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest( &argc, argv );

    return RUN_ALL_TESTS();
}
