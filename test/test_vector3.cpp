#include <gtest/gtest.h>

#include "penne/vector3.hxx"
#include "penne/vector3_utils.hxx"

class Vector3Test : public ::testing::Test {
 protected:
    Vector3Test();
    virtual ~Vector3Test();

    virtual void SetUp();

    virtual void TearDown();
    
};

Vector3Test::Vector3Test() {
}

Vector3Test::~Vector3Test() {
}

void Vector3Test::SetUp() {
}

void Vector3Test::TearDown() {
}

TEST_F(Vector3Test, sanity_test) {
    penne::vector3<double> v( 1, 2, 3 );

    EXPECT_EQ( 1, v.x );
    EXPECT_EQ( 2, v.y );
    EXPECT_EQ( 3, v.z );
}

TEST_F(Vector3Test, test_max) {
    penne::vector3<double> v( 1, 2, 3 );

    EXPECT_EQ( 3, penne::max( v ) );
}

TEST_F(Vector3Test, test_min) {
    penne::vector3<double> v( 1, 2, 3 );

    EXPECT_EQ( 1, penne::min( v ) );
}

TEST_F(Vector3Test, test_eq_assign) {
    penne::vector3<double> v( 1, 2, 3 );

    penne::vector3<double> u( v );

    EXPECT_EQ( u.x, v.x );
    EXPECT_EQ( u.y, v.y );
    EXPECT_EQ( u.z, v.z );
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest( &argc, argv );

    return RUN_ALL_TESTS();
}
