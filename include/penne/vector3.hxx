#pragma once

#include <valarray>

namespace penne {


    /*!
     *
     */
    template <typename _T>
    struct vector3 {
        using value_type = _T;

        value_type x;
        value_type y;
        value_type z;

        /*!
         *
         */
        vector3() : x(0), y(0), z(0) {}
        /*!
         *
         */
        vector3(value_type x, value_type y, value_type z)
            : x(x), y(y), z(z) {}
        /*!
         *
         */
        template <typename _U>
        vector3(_U x, _U y, _U z)
            : x( static_cast<value_type>( x ) ),
              y( static_cast<value_type>( y ) ),
              z( static_cast<value_type>( z ) ) {}
        /*!
         *
         */
        vector3(const vector3 &other)
            : x( other.x ),
              y( other.y ),
              z( other.z ) {}
        /*!
         *
         */
        template <typename _U>
        vector3(const vector3<_U> &other)
            : x( static_cast<value_type>( other.x ) ),
              y( static_cast<value_type>( other.y ) ),
              z( static_cast<value_type>( other.z ) ) {}
        /*!
         *
         */
        template <typename _U>
        vector3(const std::valarray<_U> &other)
            : x( static_cast<value_type>( other[0] ) ),
              y( static_cast<value_type>( other[1] ) ),
              z( static_cast<value_type>( other[2] ) ) {}

        /*!
         *
         */
        vector3& operator = (const vector3& other) {
            this->x = other.x;
            this->y = other.y;
            this->z = other.z;

            return *this;
        }

        /*!
         *
         */
        bool operator ==(const vector3& other) const {
            return this->x == other.x &&
                this->y == other.y &&
                this->z == other.z;
        }

        /*!
         *
         */
        bool operator !=(const vector3& other) const {
            return !( *this == other );
        }
    };


}
