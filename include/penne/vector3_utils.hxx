#pragma once

#include <algorithm>

#include "penne/vector3.hxx"


namespace penne {

    template <typename _T>
    _T max(const vector3<_T> &other) {
        return std::max( { other.x, other.y, other.z } );
    }

    template <typename _T>
    _T min(const vector3<_T> &other) {
        return std::min( { other.x, other.y, other.z } );
    }

}
