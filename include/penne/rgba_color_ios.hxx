#pragma once

#include <iostream>

#include "color.hxx"

namespace penne {
    

    template <typename _T>
    std::ostream &operator << ( std::ostream &os, const rgba_color<_T> &self ) {
        os << "("
           << self.r << ", "
           << self.g << ", "
           << self.b << ", "
           << self.a
           << ")";

        return os;
    }

    std::ostream &operator << ( std::ostream &os, const rgba_color<std::uint8_t> &self ) {
        os << "("
           << static_cast<std::int32_t>( self.r ) << ", "
           << static_cast<std::int32_t>( self.g ) << ", "
           << static_cast<std::int32_t>( self.b ) << ", "
           << static_cast<std::int32_t>( self.a )
           << ")";

        return os;
    }
}
