#pragma once

#include <iostream>

#include "penne/vector3.hxx"

namespace penne {
    

    template <typename _T>
    std::ostream &operator << ( std::ostream &os, const vecto3<_T> &self ) {
        os << "("
           << self.x << ", "
           << self.y << ", "
           << self.z 
           << ")";

        return os;
    }

    std::ostream &operator << ( std::ostream &os, const vecto3<std::uint8_t> &self ) {
        os << "("
           << static_cast<std::int32_t>( self.x ) << ", "
           << static_cast<std::int32_t>( self.y ) << ", "
           << static_cast<std::int32_t>( self.z )
           << ")";

        return os;
    }


}
