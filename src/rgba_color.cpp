#include "penne/rgba_color.hxx"


namespace penne {


    template <typename _T>
    rgba_color<_T>::rgba_color( rgba_color<_T>::value_type r, rgba_color<_T>::value_type g, rgba_color<_T>::value_type b)
        : r( r ), g( g ), b( b ), a( 1 ) {
    }
    template <>
    rgba_color<std::uint8_t>::rgba_color( rgba_color<std::uint8_t>::value_type r,
                                          rgba_color<std::uint8_t>::value_type g,
                                          rgba_color<std::uint8_t>::value_type b)
        : r(r), g(g), b(b), a(0xff) {
    }

    template <>
    rgba_color<float> rgba_color<float>::fromRGBA(std::int32_t red, std::int32_t green, std::int32_t blue, std::int32_t alpha) {
        return {
            red   / 255.0f,
            green / 255.0f,
            blue  / 255.0f,
            alpha / 255.0f,
        };
    }
    template <>
    rgba_color<double> rgba_color<double>::fromRGBA(std::int32_t red, std::int32_t green, std::int32_t blue, std::int32_t alpha) {
        return {
            red   / 255.0,
            green / 255.0,
            blue  / 255.0,
            alpha / 255.0,
        };
    }
    template <>
    rgba_color<long double> rgba_color<long double>::fromRGBA(std::int32_t red, std::int32_t green, std::int32_t blue, std::int32_t alpha) {
        return {
            red   / 255.0,
            green / 255.0,
            blue  / 255.0,
            alpha / 255.0,
        };
    }
}
