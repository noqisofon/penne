#pragma once

#include <cstdint>

namespace penne {
    /*!
     *  red, green, blue, alpha と並んだ色を表します。
     */
    template <typename _T>
    struct rgba_color {
        using value_type  = _T;
        using size_type   = std::size_t;
        using self_type   = rgba_color;

        value_type r;
        value_type g;
        value_type b;
        value_type a;

        rgba_color() : r( 0 ), g( 0 ), b( 0 ), a( 1 ) {}

        rgba_color( value_type r, value_type g, value_type b);
        rgba_color( value_type r, value_type g, value_type b, value_type a) : r( r ), g( g ), b( b ), a( a ) {}

        template <typename _U>
        rgba_color( const rgba_color<_U> other ) : r( other.r ), g( other.g ), b( other.b ), a( other.a ) {}

        /*!
         * 4 を返します。
         *
         * std::array は確保可能な最大の要素数を返しますが、このクラスは 4 固定です。
         */
        constexpr size_type maxSize() const noexcept { return 4; }

#if defined(PNN_STL_COMPLIANT)
        /*!
         * maxSize() の STL 準拠メソッドです。
         */
        constexpr size_type max_size() const noexcept { return 4; }
#endif  /* defined(PNN_STL_COMPLIANT) */

#if defined(PNN_STL_COMPLIANT)
        operator std::array<_T>() const noexcept { return { this->r, this->g, this->b, this->a }; }
#endif  /* defined(PNN_STL_COMPLIANT) */
        
        /*!
         * 4 つの 8 ビット RGBA 要素から rgba_color を作成して返します。
         */
        static rgba_color<_T> fromRGBA(std::int32_t red, std::int32_t green, std::int32_t blue, std::int32_t alpha) {
            return {
                static_cast<_T>( red ),
                static_cast<_T>( green ),
                static_cast<_T>( blue ),
                static_cast<_T>( alpha )
            };
        }
    };

    using Color         = rgba_color<std::uint8_t>;
    using ColorVolumeF  = rgba_color<float>;
    using ColorVolume   = rgba_color<double>;
}
